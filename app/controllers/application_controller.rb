class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Mahtab - using this action hello now
  def hello
    render text: "Hello from Sample App !"
  end

end
